package week6;

/*
Найдем факториал числа n рекурсивно.
На вход - n
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        int res = 1;
        for (int i = 1; i <=n; i++) {
            res = res * i;
        }
        System.out.println("Factorial (FOR LOOP): " + res);
        System.out.println("Factorial (factorial): " + factorial(n));
        System.out.println("Factorial (factorialTail): " + factorialTail(n, 1));

    }

    //3 -> 3 * (factorial(2)) <- вернулась 2 и выполнилось действие (3 * 2) = 6
    //2 -> 2 * (factorial(1)) <- вернулась 1 и выполнилось действие (2 * 1) = 2
    //1 -> 1
    public static int factorial(int n) {
        if (n <= 1) {
            return 1;
        } return n * factorial(n - 1);
    }

    // https://dzen.ru/media/id/5b7ae22633ef9b00a8cc79f3/chem-hvostovaia-rekursiia-otlichaetsia-ot-obychnoi-5e8968e9ddc8e520673dce98
    public static int factorialTail(int n, int result) {
        if (n <= 1) {
            return result;
        } return factorialTail(n-1, n * result);
    }


//    public static void recursiveTest() {
//        System.out.println("Hello World!");
//        recursiveTest();
//    }

}
